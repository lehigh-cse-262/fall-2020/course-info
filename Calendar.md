# Course Calendar

Below is a tentative schedule for the semester, broken down by week. Topics covered and the order in which they are covered is subjet to change. Any changes will be reflected here. It is your responsibility to stay up to date with this calendar over the course of this semester.


| Week                      | Monday             | Wednesday          | Friday          |
| ------------------------- | ------------------ | ------------------ | --------------- |
| Week 1 (Aug 24 - Aug 28)  | Syllabus day - [Lecture](https://youtu.be/vxu-H0QNQ88)       | Rust     | |
| Week 2 (Aug 31 - Sep 04)  | Rust     | Rust     | |
| Week 3 (Sep 07 - Sep 11)  | Rust     | Rust     | |
| Week 4 (Sep 14 - Sep 18)  | Haskell     | Haskell     | |
| Week 5 (Sep 21 - Sep 25)  | Haskell     | Haskell     | |
| Week 6 (Sep 28 - Oct 02)  | Haskell     | Haskell     | Midterm Review       |
| Week 7 (Oct 05 - Oct 09)  | Lisp     | Lisp     | |
| Week 8 (Oct 12 - Oct 16)  | Lisp     | Lisp     | |
| Week 9 (Oct 19 - Oct 23)  | Lisp     | Lisp    | |
| Week 10 (Oct 26 - Oct 30) | Prolog    | Prolog     | |
| Week 11 (Nov 02 - Nov 06) | Prolog     | Prolog     | |
| Week 12 (Nov 09 - Nov 13) | Prolog     | Prolog     | |
| Week 13 (Nov 16 - Nov 20) | Mech     | Mech     | |
| Week 14 (Nov 23 - Nov 27) | Thanksgiving Break (no class) | Thanksgiving Break (no class) | Thanksgiving Break (no class) |
| Week 15 (Nov 30 - Dec 04) | Mech    | Mech     | Final Review |

For your reference: [Lehigh University Academic Calendar](https://ras.lehigh.edu/content/current-students/academic-calendar)

Important dates:

- First day of class: August 24
- Last day to withdraw with a "W": November 6
- Last day of class: December 4
