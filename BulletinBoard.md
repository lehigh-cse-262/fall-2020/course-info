# CSE 262 - Bulletin Board

## :computer: Homework

- [ ] 11/30 - [Homework 9](https://gitlab.com/lehigh-cse262/fall-2020/assignments/homework-9) - Prolog Checkers
- [x] 11/09 - [Homework 8](https://gitlab.com/lehigh-cse262/fall-2020/assignments/homework-8) - Haskell Puzzlers
- [x] 10/26 - [Homework 7](https://gitlab.com/lehigh-cse262/fall-2020/assignments/homework-7) - Writing a Parser
- [x] 10/14 - [Homework 6](https://gitlab.com/lehigh-cse262/fall-2020/assignments/homework-6) - The Lambda Calculus
- [x] 09/30 - [Homework 5](https://gitlab.com/lehigh-cse262/fall-2020/assignments/homework-5) - Clojure Fundamentals
- [x] 09/22 - [Homework 4](https://gitlab.com/lehigh-cse262/fall-2020/assignments/homework-4) - Writing a Grammar
- [x] 09/15 - [Homework 3](https://gitlab.com/lehigh-cse262/fall-2020/assignments/homework-3) - Writing a Lexer
- [x] 09/07 - [Homework 2](https://gitlab.com/lehigh-cse262/fall-2020/assignments/homework-2) - Rust Fundamentals
- [x] 08/31 - [Homework 1](https://gitlab.com/lehigh-cse262/fall-2020/assignments/homework-1) - Guessing game in Rust
- [x] 08/31 - [Homework 0](https://gitlab.com/lehigh-cse262/fall-2020/course-info/-/blob/master/Homework-0.md) - Sign up for Gitlab

## :checkered_flag: Quizzes and Exams

- [ ] 11/17 - [Quiz 10](https://gitlab.com/lehigh-cse262/fall-2020/assignments/quiz-10)
- [x] 11/10 - [Quiz 9](https://gitlab.com/lehigh-cse262/fall-2020/assignments/quiz-9)
- [x] 11/04 - [Quiz 8](https://gitlab.com/lehigh-cse262/fall-2020/assignments/quiz-8)
- [x] 10/20 - [Quiz 7](https://gitlab.com/lehigh-cse262/fall-2020/assignments/quiz-7)
- [x] 10/14 - [Quiz 6](https://gitlab.com/lehigh-cse262/fall-2020/assignments/quiz-6)
- [x] 10/06 - [Midterm Exam](https://gitlab.com/lehigh-cse262/fall-2020/assignments/midterm)
- [x] 09/28 - [Quiz 5](https://gitlab.com/lehigh-cse262/fall-2020/assignments/quiz-5)
- [x] 09/21 - [Quiz 4](https://gitlab.com/lehigh-cse262/fall-2020/assignments/quiz-4)
- [x] 09/13 - [Quiz 3](https://gitlab.com/lehigh-cse262/fall-2020/assignments/quiz-3)
- [x] 09/05 - [Quiz 2](https://gitlab.com/lehigh-cse262/fall-2020/assignments/quiz-2)
- [x] 08/28 - [Quiz 1](https://gitlab.com/lehigh-cse262/fall-2020/assignments/quiz-1)

## :books: Readings

### Week 12

- [Alan Kay - Programming Languages & Programming](https://youtu.be/prIwpKL57dM)

### Week 9

- [LYAH](http://learnyouahaskell.com/chapters) - Chapters 5,6,11,12
- [Recursive Functions of Symbolic Expressions and Their Computation by Machine, Part I](http://www-formal.stanford.edu/jmc/recursive.pdf)(optional)

### Week 8

- [LYAH](http://learnyouahaskell.com/chapters) - Chapters 1 - 4

### Week 7

- [PL Pragmatics](https://booksite.elsevier.com/9780124104099/content/Sections%20and%20Sub-sections/Scott%204e_Supplementary%20Sections.pdf) - Chapter 11.7.1

### Week 5

- [CFTBAT](https://www.braveclojure.com/clojure-for-the-brave-and-true/) - Chapters 1 - 5

### Week 4

- [Crafting Interpreters](https://craftinginterpreters.com/contents.html) - Chapters 5, 6

### Week 3

- [Crafting Interpreters](https://craftinginterpreters.com/contents.html) - Chapters 1 - 4

### Week 2

- [The Rust Book](https://doc.rust-lang.org/stable/book/ch03-00-common-programming-concepts.html) - Chapters 3 - 10. The way I would approach this reading is to do it side-by-side with the homework. Go to the associated section in the book and use it to help you solve the problems. It's not important if you don't read every word in all these chapters. Read enough to help you pass the HW tests.
- [std::boxed](https://doc.rust-lang.org/std/boxed/index.html)
- [std::rc](https://doc.rust-lang.org/std/rc/index.html)
- [std::cell](https://doc.rust-lang.org/std/cell/index.html)

### Week 1

- [The Missing Semester](https://missing.csail.mit.edu) - Chapters 1, 2, 5, 6
- [RustConf 2020 Keynote](https://youtu.be/ESXMg9OzWrQ?t=1246) - [Slides](https://docs.google.com/presentation/d/e/2PACX-1vSA_hS_o_sOgosYSbT5MnasFBSYxTLCJWjjTX8lqoKm5P8AqAp9wSIa9uYzfd60yFrm1DCjU_dI3AxC/pub)
- [Rust for Non Systems Programmers](https://youtu.be/ESXMg9OzWrQ?t=23842) - [Slides](https://becca.ooo/rustconf/2020/)
- [The Rust Book](https://doc.rust-lang.org/stable/book/ch01-00-getting-started.html) - Chapters 1, 2

## :vhs: Lectures - [Playlist](https://www.youtube.com/playlist?list=PL4A2v89SXU3Trn3mBoocRdPwDULMyk_id)
https://repl.it/repls/TreasuredWrithingOpentracker#main.hs

| Item                      | Date              | Content          | Links          |
| ------------------------- | ------------------ | ------------------ | --------------- |
| Recitation 12 | 11/13 | More Future Programming Languages | [Video](https://drive.google.com/file/d/18kb_1wN7pNJHD6qng-cj21cLhWIr8Bum/view?usp=sharing)
| Lecture 22 | 11/11 | Future Programming Languages | [Video](https://youtu.be/opEMn44P750) [Slides](https://gitlab.com/lehigh-cse262/fall-2020/course-info/-/raw/master/slides/CSE_262___Lecture_22.pptx) [Code](https://repl.it/repls/TreasuredWrithingOpentracker#main.hs)
| Lecture 21 | 11/09 | A Prolog Example | [Video](https://youtu.be/jVA42Fd3ydo) [Slides](https://gitlab.com/lehigh-cse262/fall-2020/course-info/-/raw/master/slides/CSE_262___Lecture_17.pptx) [Code](https://repl.it/repls/TreasuredWrithingOpentracker#main.hs)
| Recitation 11 | 11/06 | Prolog, HW 8 | [Video](https://drive.google.com/file/d/1foyQRzA4Wz2-w06KA2xLCoxAHA8gO82J/view?usp=sharing)
| Lecture 20 | 11/04 | Execution of Prolog Programs | [Video](https://youtu.be/tl507tRfgCg) [Slides](https://gitlab.com/lehigh-cse262/fall-2020/course-info/-/raw/master/slides/CSE_262___Lecture_17.pptx) [Code](https://repl.it/repls/TreasuredWrithingOpentracker#main.hs)
| Lecture 19 | 11/02 | First Order Predicate Logic | [Video](https://youtu.be/EuV2nO_hVzE) [Slides](https://gitlab.com/lehigh-cse262/fall-2020/course-info/-/raw/master/slides/CSE_262___Lecture_17.pptx) [Code](https://repl.it/repls/TreasuredWrithingOpentracker#main.hs)
| Recitation 10 | 10/30 | Homework 7 Solution | [Video](https://drive.google.com/file/d/1YHL3AG0nTVGRDqqZqZC4DrJGDw45_ybw/view?usp=sharing)
| Lecture 18 | 10/28 | Introduction of Logic Programming and Prolog | [Video](https://youtu.be/FU18qwuX7lo) [Slides](https://gitlab.com/lehigh-cse262/fall-2020/course-info/-/raw/master/slides/CSE_262___Lecture_17.pptx) [Code](https://repl.it/repls/TreasuredWrithingOpentracker#main.hs)
| Lecture 17 | 10/26 | Recursion in Haskell | [Video](https://youtu.be/jVA42Fd3ydo) [Slides](https://gitlab.com/lehigh-cse262/fall-2020/course-info/-/raw/master/slides/CSE_262___Lecture_17.pptx) [Code](https://repl.it/repls/TreasuredWrithingOpentracker#main.hs)
| Recitation 09 | 10/23 | Homework 7 | [Video](https://drive.google.com/file/d/1dkbaIOU5VmUSX8yNxMTl2yqmILIGS_J8/view?usp=sharing)
| Lecture 16 | 10/21 | Functors, Applicative Functors, and Monads | [Video](https://youtu.be/2xyxtkggCKA) [Slides](https://gitlab.com/lehigh-cse262/fall-2020/course-info/-/raw/master/slides/CSE_262___Lecture_16.pptx)
 | Lecture 15 | 10/19 | Haskell cont. | [Video](https://youtu.be/qtlqRTBk6WE) [Slides](https://gitlab.com/lehigh-cse262/fall-2020/course-info/-/raw/master/slides/CSE_262___Lecture_15.pptx)
 | Recitation 08 | 10/16 | Homework 6 and Haskell | [Video](https://drive.google.com/file/d/1aIAKZa9VAzZWw38BvoiY6n7g4kJhDj9v/view?usp=sharing)
 | Lecture 14 | 10/14 | Introduction to Haskell | [Video](https://youtu.be/6SukBTdh5sI)  [Slides](https://gitlab.com/lehigh-cse262/fall-2020/course-info/-/raw/master/slides/CSE_262___Lecture_14.pptx)
|Lecture 13 | 10/12 | The Lambda Calculus cont. | [Video](https://youtu.be/6SukBTdh5sI) [Slides](https://gitlab.com/lehigh-cse262/fall-2020/course-info/-/raw/master/slides/CSE_262___Lecture_13.pptx)
|Recitation 07 | 10/09 | Midterm Answers and Lambda Calculus | [Video](https://drive.google.com/file/d/1YgxxJUXRpytDfOXVKFLophltA07J2gB2/view?usp=sharing)
|Lecture 12 | 10/07 | The Lambda Calculus | [Video](https://youtu.be/cCcRtuYSnVo) [Slides](https://gitlab.com/lehigh-cse262/fall-2020/course-info/-/raw/master/slides/CSE_262___Lecture_12.pptx)
|Recitation 06 | 10/06 | Midterm Review II | [Video](https://drive.google.com/file/d/1FQM3xHm3jWepxZhIuftlWKIgMh2scMCX/view?usp=sharing)
|Review | 09/30 | Midterm Review I | [Video](https://drive.google.com/file/d/1DvOw8T_JTuhKaeV_jUJ-zkVnsgNt0UNL/view?usp=sharing)
|Lecture 11 | 09/28 | Macros | [Video](https://youtu.be/J98P2ja7zIM) [Slides](https://gitlab.com/lehigh-cse262/fall-2020/course-info/-/raw/master/slides/CSE_262___Lecture_11.pptx)
|Recitation 05 | 09/25 | Clojure, Exam | [Video](https://drive.google.com/file/d/1GkTTrVhqJOXr93ZoBmcD7ZVkDnHFVRAE/view?usp=sharing)
|Lecture 10 | 09/23 | Functional Programming in Clojure | [Video](https://youtu.be/SsYiF3vG7uU) [Slides](https://gitlab.com/lehigh-cse262/fall-2020/course-info/-/raw/master/slides/CSE_262___Lecture_10.pptx)
|Lecture 09 | 09/21 | Introduction to Clojure and Lisp | [Video](https://youtu.be/UBy_SUjeF-s) [Slides](https://gitlab.com/lehigh-cse262/fall-2020/course-info/-/raw/master/slides/CSE_262___Lecture_9.pptx)
|Recitation 04 | 09/18 | Homework 3 and Writing Grammars | [Video](https://drive.google.com/file/d/18Tl7C69jDO-qjGCB2xbbwgd93TKHDGK3/view?usp=sharing)
|Lecture 08 | 09/16 | Grammars and Parse Trees | [Video](https://youtu.be/s-gZknhEtFU) [Slides](https://gitlab.com/lehigh-cse262/fall-2020/course-info/-/raw/master/slides/CSE_262___Lecture_8.pptx)
|Lecture 07 | 09/14 | Lexing and Parsing cont. | [Video](https://youtu.be/VtFyxyxbwxc) [Code](https://repl.it/repls/OldlaceFoolhardyDatabases#main.rs)
|Recitation 03 | 09/11 | Lexing, Parsing, Homework 3 | [Video](https://drive.google.com/file/d/1cx3XwFe49eIC3DPEqxjXF-OZgvL-4Jnz/view?usp=sharing)
|Lecture 06 | 09/09 | Lexing and Parsing | [Video](https://youtu.be/723hM6befjc) [Slides](https://gitlab.com/lehigh-cse262/fall-2020/course-info/-/raw/master/slides/CSE_262___Lecture_6.pptx)
|Lecture 05 | 09/07 | Introduction to Compilers | [Video](https://youtu.be/sIGxZQvwtPw) [Slides](https://gitlab.com/lehigh-cse262/fall-2020/course-info/-/raw/master/slides/CSE_262___Lecture_5.pptx)
|Recitation 02 | 09/04 | Rust cont. | [Video](https://drive.google.com/file/d/19iBa-jEu5N2tSJuus3owUWB_Upt7TTGB/view?usp=sharing)
|Lecture 04 | 09/02 | Rust Fundamentals cont. | [Video](https://youtu.be/cRq18ZABP8w) [Code](https://repl.it/repls/ExcitingCarpalAlphatest#linkedlist/src/bin/main.rs)
|Lecture 03 | 08/31 | Programming in Rust | [Video](https://youtu.be/tfxAOGwM1qI) [Code](https://repl.it/repls/OrangeTintedCubase#linkedlist/src/bin/main.rs)
|Recitation 01 | 08/28 | Rust, cargo, git | [Video](https://drive.google.com/file/d/1qGHwkjQPws4RWhI-63RsAlBJQGnt69lO/view?usp=sharing)
|Lecture 02 | 08/26 | Introduction to Rust and Course Tools | [Video](https://youtu.be/0zIx9MqDu88) [Slides](https://gitlab.com/lehigh-cse262/fall-2020/course-info/-/raw/master/slides/CSE%20262%20%E2%80%93%20Lecture%202.pptx)
|Lecture 01 | 08/24 | Course Introduction | [Video](https://youtu.be/vxu-H0QNQ88)

## Language of the Week

- Week 12 - [Smalltalk](https://en.wikipedia.org/wiki/Smalltalk)
- Week 11 - [Lucid](https://en.wikipedia.org/wiki/Lucid_(programming_language))
- Week 10 - [Datalog](https://en.wikipedia.org/wiki/Datalog)
- Week 9 - [ML](https://en.wikipedia.org/wiki/ML_(programming_language))
- Week 8 - [Erlang](https://en.wikipedia.org/wiki/Erlang_(programming_language))
- Week 7 - [Elm](https://en.wikipedia.org/wiki/Elm_(programming_language))
- Week 5 - [Lisp](https://en.wikipedia.org/wiki/Lisp_(programming_language))
- Week 4 - [Cobol](https://en.wikipedia.org/wiki/Cobol)
- Week 3 - [Fortran](https://en.wikipedia.org/wiki/Fortran)
- Week 2 - [EBNF](https://en.wikipedia.org/wiki/Extended_Backus–Naur_form)
- Week 1 - [TOML](https://en.wikipedia.org/wiki/TOML)
